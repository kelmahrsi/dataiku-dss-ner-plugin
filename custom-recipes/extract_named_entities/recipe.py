from __future__ import unicode_literals

import dataiku
import pandas as pd, numpy as np
from dataiku import pandasutils as pdu

import en_core_web_md

from dataiku.customrecipe import *

# Inputs and outputs are defined by roles. In the recipe's I/O tab, the user can associate one
# or more dataset to each input and output role.
# Roles need to be defined in recipe.json, in the inputRoles and outputRoles fields.

# To  retrieve the datasets of an input role named 'input_A' as an array of dataset names:
INPUT_DATASET_NAME = get_input_names_for_role('input_dataset')[0]

# For outputs, the process is the same:
OUTPUT_DATASET_NAME = get_output_names_for_role('output_dataset')[0]


# The configuration consists of the parameters set up by the user in the recipe Settings tab.

# Parameters must be added to the recipe.json file so that DSS can prompt the user for values in
# the Settings tab of the recipe. The field "params" holds a list of all the params for wich the
# user will be prompted for values.

# The configuration is simply a map of parameters, and retrieving the value of one of them is simply:

# For optional parameters, you should provide a default value in case the parameter is not present:
TEXT_COL_NAME = get_recipe_config().get('text_column')

entities_map = {}
entities_map['PERSON'] = get_recipe_config().get('person_column', None)
entities_map['ORG'] = get_recipe_config().get('org_column', None)
entities_map['GPE'] = get_recipe_config().get('gpe_column', None)
entities_map['FAC'] = get_recipe_config().get('fac_column', None)
entities_map['LOC'] = get_recipe_config().get('loc_column', None)
entities_map['DATE'] = get_recipe_config().get('date_column', None)
entities_map['TIME'] = get_recipe_config().get('time_column', None)
entities_map['EVENT'] = get_recipe_config().get('event_column', None)
entities_map['PRODUCT'] = get_recipe_config().get('product_column', None)
entities_map['WORK_OF_ART'] = get_recipe_config().get('work_of_art_column', None)
entities_map = { key : value for (key, value) in entities_map.items() if value is not None }

nlp = en_core_web_md.load(disable=["parser", "tagger"])

def extract_named_entities(text, entity_types_list = ['PERSON', 'ORG']):
    entities_dict = dict((entity_type, []) for entity_type in entity_types_list)
    for entity in nlp(text.decode('utf-8')).ents:
        if entity.label_ in entity_types_list: # Entity of interest
            entities_dict[entity.label_].append(entity.text)
            
    # Clean the entities dict
    for entity_type in entities_dict:
        if len(entities_dict[entity_type]) == 0:
            entities_dict[entity_type] = None
        else:
            entities_dict[entity_type] = list(set(entities_dict[entity_type]))
    return entities_dict

ds_in = dataiku.Dataset(INPUT_DATASET_NAME)
ds_out = dataiku.Dataset(OUTPUT_DATASET_NAME)

df_in = ds_in.get_dataframe()
df_out = df_in
additional_columns = pd.DataFrame.from_records(df_in[TEXT_COL_NAME].apply(extract_named_entities, entity_types_list = entities_map.keys()))
additional_columns.columns = [entities_map[column] for column in additional_columns.columns]
df_out =  pd.concat([df_out, additional_columns], axis = 1)
ds_out.write_with_schema(df_out)

# ds_out.write_schema(ds_in.get_config()['schema']['columns'])
# with ds_out.get_writer() as writer:
#    for row in ds_in.iter_rows():
#       writer.write_row_dict(row)
# Compute recipe outputs from inputs
# TODO: Replace this part by your actual code that computes the output, as a Pandas dataframe
# NB: DSS also supports other kinds of APIs for reading and writing data. Please see doc.

# df_out = df_in # For this sample code, simply copy input to output


# Write recipe outputs