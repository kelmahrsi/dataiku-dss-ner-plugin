# Dataiku DSS Named Entity Recognition (NER) Plugin

## Overview

This plugin provides a Recipe for extracting named entities (names, locations, dates and times, etc.) from a column in a dataset. The plugin is based on [spaCy](https://spacy.io) and only supports English.

## Usage

On the flow screen of your project, click on the "+ Recipe" button and select "Named Entity Recognizer".

![NER plugin flow](img/plugin_flow.png)

On the following screen, select the "Extract Named Entities" recipe.

![NER plugin recipe](img/plugin_recipe.png)

Then, specify your input dataset and output dataset.

![NER plugin datasets](img/plugin_datasets.png)

Finally, indicate the text column from which you wish to extract named entities (mandatory) and provide column names for entity types that you want to extract. You can leave fields blank if you do not wish to extract the corresponding named entities. Click the "Run" button to launch the named entity recognition job.

![NER plugin column selection](img/plugin_column_selection.png)

The following table describes the entity types that can be extracted using the plugin (based on the [full list of entity types recognized by spaCy](https://spacy.io/api/annotation#named-entities)).

| Entity type | Description |
|:----|:----|
|`PERSON`	|People, including fictional. |
|`FAC`	|Buildings, airports, highways, bridges, etc.|
|`ORG`	|Companies, agencies, institutions, etc.|
|`GPE`	|Countries, cities, states.|
|`LOC`	|Non-GPE locations, mountain ranges, bodies of water.|
|`PRODUCT`	|Objects, vehicles, foods, etc. (Not services.)|
|`EVENT`	|Named hurricanes, battles, wars, sports events, etc.|
|`WORK_OF_ART`	|Titles of books, songs, etc.|
|`DATE`	|Absolute or relative dates or periods.|
|`TIME`	|Times smaller than a day.|

## Changelog

### Version 0.1.0 (2019-07-23)

- Initial release

## Licences

[spaCy](https://spacy.io/) is provided under the MIT Licence and is the property of [ExplosionAI GmbH](https://explosion.ai/). [Dataiku Data Science Studio (DSS)](https://www.dataiku.com/dss/) is the property of [Dataiku Inc](https://www.dataiku.com/company/).
